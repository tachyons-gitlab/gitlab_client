# GitlabClient

Opiniated and minimal Gitlab client for ruby with zero dependencies

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab_client'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install gitlab_client

## Usage

### Rest API 


```ruby
  GitlabClient::Rest.new(private_token: '<TOKEN>').call( path: 'api/v4/version')
```

```ruby
client = GitlabClient::Rest.new(private_token: '<TOKEN>')
response = client.call( path: 'api/v4/projects?owned=true&with_programming_language=ruby')
puts response.body
while response.links['next']
  puts response.call(links['next'])
end
```

### Graphql API 

```ruby
  query = '{ projects { edges { node { id name description } } } }'
  GitlabClient::Graphql.new(private_token: '<TOKEN>').call(query: query)
```


### Difference between Gitlab gem and GitlabClient gem 

- Gitlab client gem is thin, it does not include helper methods per endpoint, nor CLI
- Gitlab client gem also supports Graphql WIP
- Gitlab client gem have built in mechanism for pagination TODO
- Gitlab client gem does not have global state, you can connect to more than one Gitlab instances from same code safely 
- Gitlab client gem does not depend on other gems, ie no worry about dependency updates

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/tachyons/gitlab_client. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/tachyons/gitlab_client/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GitlabClient project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/tachyons/gitlab_client/blob/master/CODE_OF_CONDUCT.md).
