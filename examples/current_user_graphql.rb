# frozen_string_literal: true

require_relative "../lib/gitlab_client"

client = GitlabClient::Graphql.new(private_token: ENV.fetch("PERSONAL_ACCESS_TOKEN", nil))
query = "{
  currentUser {
    id
    name
    email
  }
}"

puts client.call(query: query)
