# frozen_string_literal: true

require_relative "../lib/gitlab_client"

client = GitlabClient::Rest.new(access_token: ENV.fetch("PERSONAL_ACCESS_TOKEN", nil))

response = client.post(path: "api/v4/projects/", data: {
                         name: "new project #{Random.rand(500)}"
                       })

response = client.put(path: "api/v4/projects/#{response.body["id"]}", data: {
                        name: "new project #{Random.rand(500)}"
                      })

response = client.delete(path: "api/v4/projects/#{response.body["id"]}")

puts response.body
