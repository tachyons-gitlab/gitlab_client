# frozen_string_literal: true

require_relative "../lib/gitlab_client"

client = GitlabClient::Rest.new(access_token: ENV.fetch("PERSONAL_ACCESS_TOKEN", nil))

response = client.call(path: "api/v4/projects?pagination=keyset&per_page=50&order_by=id&sort=asc")

loop do
  break unless response.success?

  response.body.each do |project|
    puts project["name"]
  end
  response = response.next!(client)
end
