# frozen_string_literal: true
# typed: true

require "net/http"
require "json"
require "uri"

module GitlabClient
  class Rest
    class Response
      LINK_REGEX = /<([^>]+)>; rel="([^"]+)"/
      attr_accessor :body, :headers, :status_code, :raw_response

      def initialize(body:, headers:, status_code:, raw_response:)
        @body = body
        @headers = headers
        @status_code = status_code
        @raw_response = raw_response
      end

      def success?
        (200..299).cover?(@status_code)
      end

      def links
        return {} unless headers["link"]

        @links ||= headers["link"].first.split(",").to_h { |l| l.match(LINK_REGEX).captures }.invert
      end

      def next!(client)
        return nil unless next?

        client.call(path: next_link)
      end

      private

      def next_link
        links["next"]
      end

      def next?
        !next_link.nil?
      end
    end
    DEFAULT_HEADERS = { "Accept" => "application/json",
                        "Content-Type" => "application/json",
                        "User-Agent" => "gitlab_client #{GitlabClient::VERSION}" }.freeze
    MAXIMUM_REDIRECTS = 5
    attr_reader :access_token, :refresh_token, :private_token

    def initialize(access_token: nil, refresh_token: nil, private_token: nil, host: "https://gitlab.com")
      @host = host
      @access_token = access_token
      @refresh_token = refresh_token
      @private_token = private_token
    end

    # rubocop:disable  Metrics/AbcSize
    def call(path:, method: "GET", custom_headers: {}, data: {}, params: {})
      raise ArgumentError if path.nil?

      path = URI(path)
      path.query = URI.encode_www_form(params)

      req_headers = headers.merge(custom_headers)
      uri = URI.join(@host, path.to_s)
      case method
      when "GET"
        response = Net::HTTP.get_response(uri, req_headers)
      when "POST"
        response = Net::HTTP.post(uri, data.to_json, headers)
      when "PUT"
        response = http.put(uri.to_s, data.to_json, headers)
      when "DELETE"
        response = http.delete(uri.to_s, headers)
      end

      response
        .then { |raw_response| build_response(raw_response) }
        .then { |res| handle_pagination(res) }
    end
    # rubocop:enable Metrics/AbcSize

    #
    # Sends GET request to GitLab API
    #
    # @param [<String>] path path to send request to
    # @param [<Hash>] custom_headers custom headers to be included
    # @param [<Hash>] params query params to be included
    #
    # @return [<GitLabClient::Rest::Response>]
    #
    def get(path:, custom_headers: {}, params: {})
      call(path: path, custom_headers: custom_headers, method: "GET", data: {}, params: params)
    end

    # Sends POST request to GitLab API
    #
    # @param [<String>] path path to send request to
    # @param [<Hash>] data data to be included
    # @param [<Hash>] custom_headers custom headers to be included
    # @param [<Hash>] params query params to be included
    # @return [<GitLabClient::Rest::Response>]

    def post(path:, data:, custom_headers: {}, params: {})
      call(path: path, custom_headers: custom_headers, method: "POST", data: data, params: params)
    end

    # Sends PUT request to GitLab API
    #
    # @param [<String>] path path to send request to
    # @param [<Hash>] custom_headers custom headers to be included
    # @param [<Hash>] params query params to be included
    # @return [<GitLabClient::Rest::Response>]

    def put(path:, data:, custom_headers: {}, params: {})
      call(path: path, custom_headers: custom_headers, method: "PUT", data: data, params: params)
    end

    # Sends DELETE request to GitLab API
    #
    # @param [<String>] path path to send request to
    # @param [<Hash>] custom_headers custom headers to be included
    # @param [<Hash>] params query params to be included
    # @return [<GitLabClient::Rest::Response>]

    def delete(path:, custom_headers: {}, params: {})
      call(path: path, custom_headers: custom_headers, method: "DELETE", data: {}, params: params)
    end

    private

    def http
      host = URI(@host).host || ""
      http = Net::HTTP.new(host, URI(@host).port)
      http.use_ssl = true
      http
    end

    def build_response(raw_response)
      if raw_response.content_type == "application/json"
        parsed_body = ::JSON.parse(raw_response.body)
      elsif raw_response.content_type == "text/plain"
        raw_response.body
      else
        parsed_body = nil
      end
      Response.new(body: parsed_body,
                   headers: raw_response.to_hash,
                   status_code: raw_response.code.to_i,
                   raw_response: raw_response)
    end

    def handle_pagination(response)
      response
    end

    def headers
      return DEFAULT_HEADERS.merge({ "PRIVATE-TOKEN" => private_token }) if private_token
      return DEFAULT_HEADERS.merge({ "Authorization" => "Bearer #{access_token}" }) if access_token

      {}
    end
  end
end
