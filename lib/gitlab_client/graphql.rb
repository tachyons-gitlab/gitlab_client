# frozen_string_literal: true
# typed: true

require "net/http"
require "json"

module GitlabClient
  # Gitlab client
  class Graphql
    DEFAULT_HEADERS = { "Accept" => "application/json", "Content-Type" => "application/json" }.freeze
    GRAPHQL_ENDPOINT = "api/graphql"
    MAXIMUM_REDIRECTS = 5
    attr_reader :access_token, :refresh_token, :private_token

    def initialize(access_token: nil, refresh_token: nil, private_token: nil, host: "https://gitlab.com/")
      @host = host
      @access_token = access_token
      @refresh_token = refresh_token
      @private_token = private_token
    end

    def call(query:, variables: {})
      response = Net::HTTP.post(uri, { query: query, variables: variables }.to_json, headers)

      response
        .then { |res| handle_error(res) }
        .then { |res| JSON.parse(res.body) }
    end

    private

    def handle_error(response)
      case response
      when Net::HTTPClientError
        raise Error, JSON.parse(response.body)
      end
      response
    end

    def headers
      return DEFAULT_HEADERS.merge({ "PRIVATE-TOKEN" => private_token }) if private_token
      return DEFAULT_HEADERS.merge({ "Authorization" => "Bearer #{access_token}" }) if access_token

      {}
    end

    def uri
      URI.join(@host, GRAPHQL_ENDPOINT)
    end
  end
end
