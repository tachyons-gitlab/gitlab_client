# frozen_string_literal: true
# typed: true

require_relative "gitlab_client/version"
require_relative "gitlab_client/rest"
require_relative "gitlab_client/graphql"

module GitlabClient
  class Error < StandardError; end
end
