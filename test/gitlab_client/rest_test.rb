# frozen_string_literal: true

require "test_helper"

class RestClientTest < Minitest::Test
  def test_get_version
    client = GitlabClient::Rest.new(private_token: "token")
    stub_request(:get, "https://gitlab.com/api/v4/version")
      .with(
        headers: {
          "Accept" => "application/json",
          "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
          "Host" => "gitlab.com",
          "Private-Token" => "token"
        }
      )
      .to_return(status: 200, body: '{"version":"15.8.0-pre","revision":"f5813e39c2b","kas":
                 {"enabled":true,"externalUrl":"wss://kas.gitlab.com","version":"15.8.0-rc2"},
                 "enterprise":true}', headers: { "Content-Type": "application/json" })
    response = client.call(path: "api/v4/version")
    assert_equal response.body,
                 { "version" => "15.8.0-pre", "revision" => "f5813e39c2b",
                   "kas" => { "enabled" => true, "externalUrl" => "wss://kas.gitlab.com", "version" => "15.8.0-rc2" },
                   "enterprise" => true }
    assert_equal response.status_code, 200
  end
end
