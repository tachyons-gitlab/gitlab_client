# frozen_string_literal: true

require "test_helper"

class GraphqlClientTest < Minitest::Test
  def test_get_version
    client = GitlabClient::Graphql.new(private_token: "token")
    query = "{ projects { edges { node { id name description } } } }"
    stub_request(:post, "https://gitlab.com/api/graphql")
      .with(
        body: "{\"query\":\"{ projects { edges { node { id name description } } } }\",\"variables\":{}}",
        headers: {
          "Accept" => "application/json",
          "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
          "Content-Type" => "application/json",
          "Host" => "gitlab.com",
          "Private-Token" => "token"
        }
      )
      .to_return(status: 200, body: "{}", headers: {})
    assert client.call(query: query), {}
  end
end
